#![allow(clippy::module_name_repetitions)]

use amethyst::core::math::Vector2;

use serde::{
    Serialize,
    Deserialize
};

//
// Constants
//

pub const PADDLE_WIDTH:  f32 = 4.0;
pub const PADDLE_HEIGHT: f32 = 16.0;

pub const MAX_SCORE: i32 = 999;

const DEFAULT_ARENA_WIDTH:  f32 = 100.0;
const DEFAULT_ARENA_HEIGHT: f32 = 100.0;

const DEFAULT_BALL_RADIUS:      f32 = 2.0;
const DEFAULT_BALL_VELOCITY_X:  f32 = 75.0;
const DEFAULT_BALL_VELOCITY_Y:  f32 = 50.0;
const DEFAULT_BALL_SPAWN_DELAY: f32 = 1.0;

//
// pub struct GameplayConfig
//

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct GameplayConfig {
    pub arena_config: ArenaConfig,
    pub ball_config:  BallConfig
}

//
// pub struct ArenaConfig
//

#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
pub struct ArenaConfig {
    pub width:  f32,
    pub height: f32
}

impl Default for ArenaConfig {
    fn default() -> Self {
        Self::new()
    }
}

impl ArenaConfig {
    pub fn new() -> Self {
        Self{
            width:  DEFAULT_ARENA_WIDTH,
            height: DEFAULT_ARENA_HEIGHT
        }
    }
}

//
// pub struct BallConfig
//

#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
pub struct BallConfig {
    pub radius:      f32,
    pub velocity:    Vector2<f32>,
    pub spawn_delay: f32
}

impl Default for BallConfig {
    fn default() -> Self {
        Self::new()
    }
}

impl BallConfig {
    pub fn new() -> Self {
        Self{
            radius:      DEFAULT_BALL_RADIUS,
            velocity:    Vector2::new(DEFAULT_BALL_VELOCITY_X, DEFAULT_BALL_VELOCITY_Y),
            spawn_delay: DEFAULT_BALL_SPAWN_DELAY
        }
    }
}
