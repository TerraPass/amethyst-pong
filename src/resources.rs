use std::mem;

use amethyst::ecs::Entity;

//
// pub enum PauseState
//

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Pause {
    Unpaused,
    Paused
}

impl Default for Pause {
    fn default() -> Self {
        Self::Unpaused
    }
}

impl Pause {
    pub fn toggle(&mut self) {
        let opposite = if *self == Pause::Unpaused {
            Pause::Paused
        } else {
            Pause::Unpaused
        };

        mem::replace(
            self,
            opposite
        );
    }
}

//
// pub struct ScoreBoard
//

#[derive(Default)]
pub struct ScoreBoard {
    pub score_left:  i32,
    pub score_right: i32
}

//
// pub struct ScoreText
//

pub struct ScoreText {
    pub score_left_text:  Entity,
    pub score_right_text: Entity
}
