use std::{
    iter::Cycle,
    vec::IntoIter
};

use amethyst::{
    assets::{
        Loader,
        AssetStorage,
        ProgressCounter
    },
    audio::{
        output::Output,
        AudioSink,
        OggFormat,
        Source,
        SourceHandle
    },
    ecs::{
        World,
        WorldExt
    }
};

use enum_map::{
    Enum,
    EnumMap,
    enum_map
};

const BOUNCE_SOUND: &str = "audio/bounce.ogg";
const SCORE_SOUND:  &str = "audio/score.ogg";

const MUSIC_TRACKS: &[&str] = &[
    "audio/music0.ogg",
    "audio/music1.ogg"
];

#[derive(Clone, Copy, Debug, Enum)]
pub enum SoundEvent {
    Bounce,
    Score
}

pub struct Sounds(EnumMap<SoundEvent, SourceHandle>);

pub struct Music(Cycle<IntoIter<SourceHandle>>);

impl Music {
    pub fn next_track(&mut self) -> Option<SourceHandle> {
        self.0.next()
    }
}

fn load_audio_track(
    loader:           &Loader,
    world:            &World,
    file:             &str,
    progress_counter: &mut ProgressCounter
) -> SourceHandle {
    loader.load(
        file,
        OggFormat,
        progress_counter,
        &world.read_resource()
    )
}

#[allow(clippy::module_name_repetitions)]
pub fn initialize_audio(
    world:            &mut World,
    progress_counter: &mut ProgressCounter
) {
    let sounds_map = {
        let loader = world.read_resource::<Loader>();

        enum_map!{
            SoundEvent::Bounce => load_audio_track(&loader, &world, BOUNCE_SOUND, &mut *progress_counter),
            SoundEvent::Score  => load_audio_track(&loader, &world, SCORE_SOUND,  &mut *progress_counter)
        }
    };

    let music_cycle = {
        let loader = world.read_resource::<Loader>();

        let mut sink = world.write_resource::<AudioSink>();
        sink.set_volume(0.25);

        MUSIC_TRACKS.iter()
            .map(|track| load_audio_track(&loader, &world, track, &mut *progress_counter))
            .collect::<Vec<_>>()
            .into_iter()
            .cycle()
    };

    world.insert(Sounds(sounds_map));
    world.insert(Music(music_cycle));
}

pub fn play_sound(
    event:   SoundEvent,
    sounds:  &Sounds,
    storage: &AssetStorage<Source>,
    output:  Option<&Output>
) {
    if let Some(ref output) = output.as_ref() {
        let source = storage.get(&sounds.0[event])
            .expect("expected source for the given sound event to exist");

        output.play_once(source, 1.0);
    }
}