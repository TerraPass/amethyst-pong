use amethyst::{
    core::{
        timing::Time,
        Transform
    },
    derive::SystemDesc,
    ecs::{
        Join,
        Read,
        ReadStorage,
        System,
        SystemData,
        WriteStorage
    },
    input::InputHandler
};

use crate::{
    config::{
        ArenaConfig,
        PADDLE_HEIGHT
    },
    components::Paddle,
    input::{
        AxisBinding,
        PongBindings
    }
};

const PADDLE_SPEED: f32 = 100.0;

#[allow(clippy::module_name_repetitions)]
#[derive(SystemDesc)]
pub struct PaddleSystem;

impl<'s> System<'s> for PaddleSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        WriteStorage<'s, Transform>,
        ReadStorage<'s, Paddle>,
        Read<'s, ArenaConfig>,
        Read<'s, InputHandler<PongBindings>>,
        Read<'s, Time>
    );

    fn run(&mut self, (mut transforms, paddles, arena_config, input, time): Self::SystemData) {
        for (paddle, transform) in (&paddles, &mut transforms).join() {
            let axis_value = input.axis_value(&AxisBinding::Paddle(paddle.side))
                .expect("expected input axis to exist");

            let translation_amount = PADDLE_SPEED * time.delta_seconds() * axis_value;

            transform.set_translation_y(
                (transform.translation().y + translation_amount)
                    .max(0.5 * PADDLE_HEIGHT)
                    .min(arena_config.height - 0.5 * PADDLE_HEIGHT)
            );
        }
    }
}
