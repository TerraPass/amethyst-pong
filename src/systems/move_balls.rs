use amethyst::{
    core::{
        math::Vector3,
        timing::Time,
        transform::Transform
    },
    derive::SystemDesc,
    ecs::prelude::{
        Join,
        Read,
        ReadStorage,
        System,
        SystemData,
        WriteStorage
    }
};

use crate::components::Ball;

#[allow(clippy::module_name_repetitions)]
#[derive(SystemDesc)]
pub struct MoveBallsSystem;

impl<'s> System<'s> for MoveBallsSystem {
    type SystemData = (
        ReadStorage<'s, Ball>,
        WriteStorage<'s, Transform>,
        Read<'s, Time>
    );

    fn run(&mut self, (balls, mut transforms, time): Self::SystemData) {
        let dt = time.delta_seconds();

        for (ball, transform) in (&balls, &mut transforms).join() {
        transform.prepend_translation(
                Vector3::new(
                    dt * ball.velocity[0],
                    dt * ball.velocity[1],
                    0.0
                )
            );
        }
    }
}
