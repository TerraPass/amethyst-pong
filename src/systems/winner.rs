use amethyst::{
    assets::AssetStorage,
    audio::{
        output::Output,
        Source
    },
    core::Transform,
    derive::SystemDesc,
    ecs::prelude::{
        Join,
        Read,
        ReadExpect,
        System,
        SystemData,
        Write,
        WriteStorage
    },
    ui::UiText
};

use crate::{
    config::{
        ArenaConfig,
        MAX_SCORE
    },
    components::Ball,
    resources::{
        ScoreBoard,
        ScoreText
    },
    audio::{
        self,
        SoundEvent,
        Sounds
    }
};

#[allow(clippy::module_name_repetitions)]
#[derive(SystemDesc)]
pub struct WinnerSystem;

impl<'s> System<'s> for WinnerSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        WriteStorage<'s, Ball>,
        WriteStorage<'s, Transform>,
        WriteStorage<'s, UiText>,
        Read<'s, ArenaConfig>,
        Write<'s, ScoreBoard>,
        ReadExpect<'s, ScoreText>,
        Read<'s, AssetStorage<Source>>,
        ReadExpect<'s, Sounds>,
        Option<Read<'s, Output>>
    );

    fn run(
        &mut self,
        (
            mut balls,
            mut transforms,
            mut ui_texts,
            arena_config,
            mut score_board,
            score_text,
            source_storage,
            sounds,
            output
        ): Self::SystemData
    ) {
        for (ball, transform) in (&mut balls, &mut transforms).join() {
            let ball_x = transform.translation().x;

            let has_scored = if ball_x < -ball.radius {
                score_board.score_right = (score_board.score_right + 1).min(MAX_SCORE);

                println!("[DEBUG] Player 1 has scored");

                true
            } else if ball_x > arena_config.width + ball.radius {
                score_board.score_left = (score_board.score_left + 1).min(MAX_SCORE);

                println!("[DEBUG] Player 2 has scored");

                true
            } else {
                false
            };

            if has_scored {
                Self::update_score_text(&mut ui_texts, &score_board, &score_text);

                audio::play_sound(
                    SoundEvent::Score,
                    &*sounds,
                    &*source_storage,
                    output.as_deref()
                );

                ball.velocity[0] *= -1.0;
                transform.set_translation_xyz(
                    0.5*arena_config.width,
                    0.5*arena_config.height,
                    0.0
                );
            }
        }
    }
}

impl WinnerSystem {
    fn update_score_text<'s>(
        ui_texts:    &mut WriteStorage<'s, UiText>,
        score_board: &Write<'s, ScoreBoard>,
        score_text:  &ReadExpect<'s, ScoreText>
    ) {
        let score_left_text = ui_texts.get_mut(score_text.score_left_text)
            .expect("expected score_left_text to exist");

        score_left_text.text  = score_board.score_left.to_string();

        let score_right_text = ui_texts.get_mut(score_text.score_right_text)
            .expect("expected score_right_text to exist");

        score_right_text.text = score_board.score_right.to_string();

    }
}
