use std::ops::Deref;

use amethyst::{
    assets::AssetStorage,
    audio::{
        output::Output,
        Source
    },
    core::{
        timing::Time,
        Transform
    },
    ecs::prelude::{
        Join,
        Read,
        ReadExpect,
        ReadStorage,
        System,
        WriteStorage
    }
};

use crate::{
    config::ArenaConfig,
    components::{
        Ball,
        Side,
        Paddle
    },
    audio::{
        self,
        SoundEvent,
        Sounds
    }
};

#[allow(clippy::module_name_repetitions)]
pub struct BounceSystem;

impl<'s> System<'s> for BounceSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        WriteStorage<'s, Ball>,
        ReadStorage<'s, Paddle>,
        ReadStorage<'s, Transform>,
        Read<'s, ArenaConfig>,
        Read<'s, Time>,
        Read<'s, AssetStorage<Source>>,
        ReadExpect<'s, Sounds>,
        Option<Read<'s, Output>>
    );

    fn run(
        &mut self,
        (
            mut balls,
            paddles,
            transforms,
            arena_config,
            time,
            source_storage,
            sounds,
            output
        ): Self::SystemData
    ) {
        for (ball, ball_transform) in (&mut balls, &transforms).join() {
            let ball_next_x = ball_transform.translation().x + ball.velocity[0]*time.delta_seconds();
            let ball_next_y = ball_transform.translation().y + ball.velocity[1]*time.delta_seconds();

            if (ball_next_y <= ball.radius && ball.velocity[1] < 0.0)
                || (ball_next_y >= arena_config.height - ball.radius && ball.velocity[1] > 0.0)
            {
                ball.velocity[1] *= -1.0;

                play_bounce_sound(&sounds, &source_storage, &output);
            }

            for (paddle, paddle_transform) in (&paddles, &transforms).join() {
                let paddle_x = paddle_transform.translation().x;
                let paddle_y = paddle_transform.translation().y;

                let paddle_left   = paddle_x - 0.5*paddle.width;
                let paddle_top    = paddle_y - 0.5*paddle.height;
                let paddle_right  = paddle_x + 0.5*paddle.width;
                let paddle_bottom = paddle_y + 0.5*paddle.height;

                let collides_vertically   = ball_next_y >= paddle_top - ball.radius
                    && ball_next_y < paddle_bottom + ball.radius;

                if !collides_vertically {
                    continue;
                }

                let collides_horizontally = match paddle.side {
                    Side::Left  => {
                        ball_next_x < paddle_right + ball.radius &&
                        ball.velocity[0] < 0.0
                    },
                    Side::Right => {
                        ball_next_x > paddle_left - ball.radius &&
                        ball.velocity[0] > 0.0
                    }
                };

                if collides_horizontally {
                    ball.velocity[0] *= -1.0;

                    play_bounce_sound(&sounds, &source_storage, &output);
                }
            }
        }
    }
}

fn play_bounce_sound(
    sounds:         &ReadExpect<'_, Sounds>,
    source_storage: &Read<'_, AssetStorage<Source>>,
    output:         &Option<Read<'_, Output>>
) {
    audio::play_sound(
        SoundEvent::Bounce,
        &*sounds,
        &*source_storage,
        output.as_ref().map(Deref::deref)
    );
}
