use amethyst::{
    assets::{
        Loader,
        Handle,
        ProgressCounter
    },
    core::transform::Transform,
    ecs::prelude::Entity,
    prelude::*,
    renderer::{
        Camera,
        SpriteRender,
        SpriteSheet
    },
    ui::{
        Anchor,
        TtfFormat,
        UiText,
        UiTransform
    }
};

use crate::{
    config::{
        ArenaConfig,
        PADDLE_WIDTH,
        BallConfig
    },
    components::{
        Side,
        Paddle,
        Ball
    },
    resources::ScoreText
};

pub fn initialize_camera(world: &mut World) {
    let (arena_width, arena_height) = retrieve_arena_dimensions(world);

    let mut transform = Transform::default();
    transform.set_translation_xyz(arena_width * 0.5, arena_height * 0.5, 1.0);

    world.create_entity()
        .with(Camera::standard_2d(arena_width, arena_height))
        .with(transform)
        .build();
}

pub fn initialize_paddles(world: &mut World, sprite_sheet: Handle<SpriteSheet>) {
    let (arena_width, arena_height) = retrieve_arena_dimensions(world);

    let mut left_transform  = Transform::default();
    let mut right_transform = Transform::default();

    let y = 0.5 * arena_height;

    left_transform.set_translation_xyz(0.5 * PADDLE_WIDTH, y, 0.0);
    right_transform.set_translation_xyz(arena_width - 0.5*PADDLE_WIDTH, y, 0.0);

    let sprite_render = SpriteRender{
        sprite_sheet,
        sprite_number: 0
    };

    world.create_entity()
        .with(Paddle::new(Side::Left))
        .with(left_transform)
        .with(sprite_render.clone())
        .build();

    world.create_entity()
        .with(Paddle::new(Side::Right))
        .with(right_transform)
        .with(sprite_render)
        .build();
}

pub fn initialize_ball(world: &mut World, sprite_sheet: Handle<SpriteSheet>) -> Entity {
    let (arena_width, arena_height) = retrieve_arena_dimensions(world);
    let ball_config: BallConfig               = *world.read_resource::<BallConfig>();

    let mut transform = Transform::default();
    transform.set_translation_xyz(0.5*arena_width, 0.5*arena_height, 0.0);

    let sprite_render = SpriteRender {
        sprite_sheet,
        sprite_number: 1
    };

    world.create_entity()
        .with(
            Ball{
                radius:   ball_config.radius,
                velocity: ball_config.velocity
            }
        )
        .with(transform)
        .with(sprite_render)
        .build()
}

pub fn initialize_scoreboard(world: &mut World, progress_counter: &mut ProgressCounter) {
    let font = world.read_resource::<Loader>().load(
        "font/square.ttf",
        TtfFormat,
        progress_counter,
        &world.read_resource()
    );

    let score_left_transform = UiTransform::new(
        String::from("score_left"),
        Anchor::TopMiddle,
        Anchor::TopMiddle,
        -50.0,
        -50.0,
        1.0,
        200.0,
        50.0
    );
    let score_right_transform = UiTransform::new(
        String::from("score_left"),
        Anchor::TopMiddle,
        Anchor::TopMiddle,
        50.0,
        -50.0,
        1.0,
        200.0,
        50.0
    );

    let score_left_text = world.create_entity()
        .with(score_left_transform)
        .with(
            UiText::new(
                font.clone(),
                String::from("0"),
                [1.0, 1.0, 1.0, 1.0],
                50.0
            )
        )
        .build();
    let score_right_text = world.create_entity()
        .with(score_right_transform)
        .with(
            UiText::new(
                font,
                String::from("0"),
                [1.0, 1.0, 1.0, 1.0],
                50.0
            )
        )
        .build();

    world.insert(
        ScoreText{
            score_left_text,
            score_right_text
        }
    );
}

//
// Service
//

fn retrieve_arena_dimensions(world: &mut World) -> (f32, f32) {
    let arena_config = &world.read_resource::<ArenaConfig>();

    (arena_config.width, arena_config.height)
}
