use amethyst::{
    core::math::Vector2,
    ecs::prelude::{
        Component,
        DenseVecStorage
    }
};

use serde::{
    Serialize,
    Deserialize
};

use crate::config::{
    PADDLE_WIDTH,
    PADDLE_HEIGHT
};

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Side {
    Left,
    Right
}

pub struct Paddle {
    pub side:   Side,
    pub width:  f32,
    pub height: f32
}

impl Paddle {
    pub fn new(side: Side) -> Self {
        Self{
            side,
            width:  PADDLE_WIDTH,
            height: PADDLE_HEIGHT
        }
    }
}

impl Component for Paddle {
    type Storage = DenseVecStorage<Self>;
}

pub struct Ball {
    pub radius:   f32,
    pub velocity: Vector2<f32>
}

impl Component for Ball {
    type Storage = DenseVecStorage<Self>;
}
