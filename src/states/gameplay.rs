use amethyst::{
    assets::Handle,
    core::timing::Time,
    ecs::prelude::Entity,
    prelude::*,
    renderer::SpriteSheet,
    ui::UiText,
    input::InputEvent
};

use crate::{
    init,
    config::BallConfig,
    resources::{
        Pause,
        ScoreBoard,
        ScoreText
    },
    input::ActionBinding,
    data::{
        PongGameData,
        PongStateData,
        PongTrans,
        PongEvent
    }
};

#[allow(clippy::module_name_repetitions)]
pub struct GameplayState {
    ball_entity:         Option<Entity>,
    ball_spawn_timer:    Option<f32>,
    sprite_sheet_handle: Handle<SpriteSheet>
}

impl State<PongGameData, PongEvent> for GameplayState {
    fn on_start(&mut self, mut data: PongStateData<'_>) {
        *data.world.entry::<Pause>().or_insert(Pause::Unpaused) = Pause::Unpaused;

        reset_scoreboard(&mut data.world);
    }

    fn handle_event(&mut self, data: PongStateData<'_>, event: PongEvent) -> PongTrans {
        if let StateEvent::Input(InputEvent::ActionPressed(action)) = &event {
            match action {
                ActionBinding::Pause   => {
                    data.world.write_resource::<Pause>().toggle();

                    Trans::None
                },
                ActionBinding::Restart => Trans::Replace(Box::new(Self::new(&data.world, self.sprite_sheet_handle.clone()))),
                ActionBinding::Quit    => Trans::Pop
            }
        } else {
            Trans::None
        }
    }

    fn update(&mut self, data: PongStateData<'_>) -> PongTrans {
        data.data.update(&data.world);

        assert!(self.ball_spawn_timer.is_none() || self.ball_spawn_timer.unwrap() >= 0.0);
        assert_eq!(self.ball_spawn_timer.is_none(), self.ball_entity.is_some());

        if let Some(mut ball_spawn_time) = self.ball_spawn_timer.take() {
            ball_spawn_time -= data.world.fetch::<Time>().delta_seconds();

            if ball_spawn_time < 0.0 {
                self.ball_entity.replace(
                    init::initialize_ball(
                        data.world,
                        self.sprite_sheet_handle.clone()
                    )
                );
            } else {
                self.ball_spawn_timer.replace(ball_spawn_time);
            }
        }

        Trans::None
    }

    fn on_stop(&mut self, data: PongStateData<'_>) {
        if let Some(ball_entity) = self.ball_entity.take() {
            data.world.delete_entity(ball_entity)
                .expect("expected ball_entity to be a valid entity at this point");
        }
    }
}

impl GameplayState {
    pub fn new(world: &World, sprite_sheet_handle: Handle<SpriteSheet>) -> Self {
        let ball_config = &world.read_resource::<BallConfig>();

        Self{
            ball_entity:      None,
            ball_spawn_timer: Some(ball_config.spawn_delay),
            sprite_sheet_handle
        }
    }
}

//
// Service
//

fn reset_scoreboard(world: &mut World) {
    const INITIAL_SCORE: i32 = 0;

    {
        let mut score_board = world.entry::<ScoreBoard>().or_insert_with(ScoreBoard::default);

        score_board.score_left  = INITIAL_SCORE;
        score_board.score_right = INITIAL_SCORE;
    }

    let (score_left_entity, score_right_entity) = {
        let score_text = world.fetch::<ScoreText>();

        (score_text.score_left_text, score_text.score_right_text)
    };

    let mut ui_text_storage = world.write_storage::<UiText>();

    for entity in &[score_left_entity, score_right_entity] {
        let score_ui_text = ui_text_storage.get_mut(*entity)
            .expect("expected UiText component to exist for entity");
        score_ui_text.text = INITIAL_SCORE.to_string();
    }
}
