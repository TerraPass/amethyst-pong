use amethyst::{
    assets::{
        AssetStorage,
        Loader,
        Handle,
        ProgressCounter
    },
    prelude::*,
    renderer::{
        ImageFormat,
        SpriteSheet,
        SpriteSheetFormat,
        Texture
    }
};

use crate::{
    audio,
    data::{
        PongGameData,
        PongStateData,
        PongTrans,
        PongEvent
    },
    init
};

use super::GameplayState;

#[allow(clippy::module_name_repetitions)]
#[derive(Default)]
pub struct LoadingState {
    progress_counter:    ProgressCounter,
    sprite_sheet_handle: Option<Handle<SpriteSheet>>
}

impl State<PongGameData, PongEvent> for LoadingState {
    fn on_start(&mut self, data: PongStateData<'_>) {
        let world = data.world;

        let sprite_sheet_handle = load_sprite_sheet(world, &mut self.progress_counter);

        //world.register::<Paddle>(); // No longer necessary due to Paddle being used by PaddleSystem
        //world.register::<Ball>();   //

        init::initialize_camera(world);
        init::initialize_paddles(world, sprite_sheet_handle.clone());
        init::initialize_scoreboard(world, &mut self.progress_counter);
        audio::initialize_audio(world, &mut self.progress_counter);

        self.sprite_sheet_handle.replace(sprite_sheet_handle);
    }

    fn update(&mut self, data: PongStateData<'_>) -> PongTrans {
        data.data.update(&data.world);

        println!(
            "[DEBUG][pong::LoadingState] Loading progress {}/{} ({} errors)",
            self.progress_counter.num_finished(),
            self.progress_counter.num_assets(),
            self.progress_counter.num_failed()
        );

        if !self.progress_counter.is_complete() {
            return Trans::None;
        }

        Trans::Replace(
            Box::new(GameplayState::new(
                &data.world,
                self.sprite_sheet_handle.take()
                    .expect("expected sprite_sheet_handle to be set at this point")
            ))
        )
    }
}

//
// Service
//

fn load_sprite_sheet(world: &mut World, progress_counter: &mut ProgressCounter) -> Handle<SpriteSheet> {
    let texture_handle = {
        let loader          = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();

        loader.load(
            "texture/pong_spritesheet.png",
            ImageFormat::default(),
            &mut *progress_counter,
            &texture_storage
        )
    };

    let loader               = world.read_resource::<Loader>();
    let sprite_sheet_storage = world.read_resource::<AssetStorage<SpriteSheet>>();

    loader.load(
        "texture/pong_spritesheet.ron",
        SpriteSheetFormat(texture_handle),
        progress_counter,
        &sprite_sheet_storage
    )
}