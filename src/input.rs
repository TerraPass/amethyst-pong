use amethyst::input::{
    BindingTypes
};

use serde::{
    Serialize,
    Deserialize
};

use crate::{
    components::Side,
    impl_display_from_debug
};

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum AxisBinding {
    Paddle(Side)
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum ActionBinding {
    Pause,
    Restart,
    Quit
}

impl_display_from_debug!(AxisBinding);
impl_display_from_debug!(ActionBinding);

#[derive(Default, Debug)]
pub struct PongBindings;

impl BindingTypes for PongBindings {
    type Axis   = AxisBinding;
    type Action = ActionBinding;
}
