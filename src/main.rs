mod init;
mod config;
mod data;
mod states;
mod components;
mod resources;
mod systems;
mod audio;
mod input;
mod macros;

use amethyst::{
    prelude::*,
    core::{
        transform::TransformBundle
    },
    renderer::{
        plugins::{RenderFlat2D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle
    },
    input::InputBundle,
    ui::{
        RenderUi,
        UiBundle
    },
    audio::{
        AudioBundle,
        DjSystemDesc
    },
    utils as amethyst_utils,
    LoggerConfig,
    StateEventReader
};

use crate::{
    config::GameplayConfig,
    audio::Music,
    states::LoadingState,
    systems::{
        PaddleSystem,
        MoveBallsSystem,
        BounceSystem,
        WinnerSystem
    },
    resources::Pause,
    input::PongBindings
};

type PongApplication<'a> = CoreApplication::<
    'a,
    GameData<'static, 'static>,
    StateEvent<PongBindings>,
    StateEventReader<PongBindings>
>;

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(LoggerConfig::default());

    let app_root             = amethyst_utils::application_root_dir()?;
    let configs_path         = app_root.join("config");
    let display_config_path  = configs_path.join("display.ron");
    let bindings_config_path = configs_path.join("bindings.ron");
    let gameplay_config_path = configs_path.join("gameplay.ron");
    let assets_dir           = app_root.join("assets");

    let game_data = GameDataBuilder::default()
        .with_bundle(TransformBundle::new())?
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
                .with_plugin(
                    RenderToWindow::from_config_path(display_config_path)?
                        .with_clear([0.0, 0.0, 0.0, 1.0]),
                )
                .with_plugin(RenderFlat2D::default())
                .with_plugin(RenderUi::default())
        )?
        .with_bundle(
            InputBundle::<PongBindings>::new()
                .with_bindings_from_file(bindings_config_path)?
        )?
        .with_bundle(
            UiBundle::<PongBindings>::new()
        )?
        .with_bundle(AudioBundle::default())?
        .with(PaddleSystem.pausable(Pause::Unpaused), "paddle_system", &["input_system"])
        .with(MoveBallsSystem.pausable(Pause::Unpaused), "move_balls_system", &[])
        .with(BounceSystem.pausable(Pause::Unpaused), "bounce_system", &["paddle_system", "move_balls_system"])
        .with(WinnerSystem.pausable(Pause::Unpaused), "winner_system", &["bounce_system"])
        .with_system_desc(
            DjSystemDesc::new(Music::next_track),
            "dj_system",
            &[]
        );

    let gameplay_config: GameplayConfig = GameplayConfig::load(gameplay_config_path)?;

    let mut game = PongApplication::build(assets_dir, LoadingState::default())?
        .with_resource(gameplay_config.arena_config)
        .with_resource(gameplay_config.ball_config)
        .build(game_data)?;

    game.run();

    Ok(())
}
