#![allow(clippy::module_name_repetitions)]

use amethyst::prelude::*;

use crate::input::PongBindings;

pub type PongGameData      = GameData<'static, 'static>;
pub type PongStateData<'a> = StateData<'a, PongGameData>;
pub type PongEvent         = StateEvent<PongBindings>;
pub type PongTrans         = Trans<PongGameData, PongEvent>;
